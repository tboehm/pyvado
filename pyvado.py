#!/usr/bin/env python3

# Copyright (c) 2019 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from glob import glob
import os
import pexpect
import sys

class pyvado:
    """
    Open a project, run synthesis, run implementation, generate a bitstream,
    and program the device.
    """
    def __init__(self, project, project_xpr=None, device='xc7a35t_0'):
        self.project = os.path.realpath(project)
        self.project_xpr = project_xpr
        if not self.project_xpr:
            possible_xprs = sorted(glob(f'{self.project}/*.xpr'))
            try:
                self.project_xpr = os.path.realpath(possible_xprs[0])
            except IndexError as e:
                raise FileNotFoundError(f'No xpr files found in {self.project}!') from e
            if len(possible_xprs) > 1:
                print(f'Warning: {len(possible_xprs)} xpr files found.')
            print(f'Selecting {self.project_xpr} for the project file')
        self.device = device
        self.prompt = 'Vivado%'
        self._tcl_shell = None
        self._bitstream_file = None
        self.project_opened = False
        self.hw_opened = False

    @property
    def tcl_shell(self):
        if not self._tcl_shell:
            print('Spawning Tcl shell for Vivado')
            self._tcl_shell = pexpect.spawn('vivado -mode tcl', logfile=sys.stdout.buffer)
            self._tcl_shell.expect(self.prompt)
        return self._tcl_shell

    def command(self, cmd):
        self.tcl_shell.sendline(cmd)
        return self.tcl_shell.expect(self.prompt, timeout=None)

    def run(self):
        self.open_project()

    def open_project(self, project=None):
        if not project:
            project = self.project
        else:
            self.project = project
        self.command(f'open_project {self.project_xpr}')
        self.command('update_compile_order -fileset sources_1')
        self.project_opened = True

    def synthesis(self, run='synth_1'):
        if not self.project_opened:
            self.open_project()
        self.command(f'reset_run {run}')
        self.command(f'launch_runs {run} -jobs 4')
        self.command(f'wait_on_run {run}')

    def implementation(self, run='impl_1'):
        if not self.project_opened:
            self.open_project()
        self.command(f'launch_runs {run} -jobs 4')
        self.command(f'wait_on_run {run}')

    def bitstream(self, run='impl_1'):
        if not self.project_opened:
            self.open_project()
        self.command(f'launch_runs {run} -to_step write_bitstream -jobs 4')
        self.command(f'wait_on_run {run}')

    def open_hw(self, device=None):
        if not self.project_opened:
            self.open_project()
        if not device:
            device = self.device
        else:
            self.device = device
        self.command('open_hw')
        self.command('connect_hw_server')
        self.command('open_hw_target')
        self.command(f'current_hw_device [get_hw_devices {device}]')
        self.command(f'refresh_hw_device -update_hw_probes false [lindex [get_hw_devices {device}] 0]')
        self.hw_opened = True

    def program(self, bitstream_file=None, device=None):
        if not self.hw_opened:
            self.open_hw(device)
        if not device:
            device = self.device
        if not bitstream_file:
            bitstream_file = self.bitstream_file
        self.command(f'set_property PROBES.FILE {{}} [get_hw_devices {device}]')
        self.command(f'set_property FULL_PROBES.FILE {{}} [get_hw_devices {device}]')
        self.command(f'set_property PROGRAM.FILE {{{bitstream_file}}} [get_hw_devices {device}]')
        self.command(f'program_hw_devices [get_hw_devices {device}]')
        self.command(f'refresh_hw_device [lindex [get_hw_devices {device}] 0]')

    @property
    def bitstream_file(self):
        if not self._bitstream_file:
            bits = sorted(glob(f'{self.project}/*.runs/*/*.bit'))
            try:
                file = bits[0]
            except IndexError as e:
                raise FileNotFoundError(f'No bitstream files found in {self.project}!') from e
            print(f'Selecting {file} for the bitstream file')
            if len(bits) > 1:
                print(f'Warning: {len(bits)} bitstream files found.')
            self._bitstream_file = file
        return self._bitstream_file

def main():
    return

if __name__ == '__main__':
    main()
