pyvado
======

Python automation of [Vivado](https://www.xilinx.com/products/design-tools/vivado.html) tasks. 

## Installation
`pyvado` requires [Python 3](https://www.python.org/downloads), [pip](https://pypi.org/project/pip), and [pexpect](https://pypi.org/project/pexpect). To install from this Git repo, I recommend the following steps.

```
$ git clone https://gitlab.com/tboehm/pyvado.git
$ pip3 install -r requirements.txt
```

Then, create a symlink in your pip `site-packages` directory that points to `pyvado`.

COMING SOON: Installation via pip

## Basic usage

You can use `pyvado` in programs as follows:

```python
import pyvado

p = pyvado.pyvado(project='.')
p.open_project()
p.synthesis()
p.implementation()
p.bitstream()
p.open_hw()
p.program()
```

Additionally, you can use `pyvado_cli` directly from the command line. This is helpful for Make targets. The following command will open the project in the current working directory, run synthesis, run implementation, generate the bitstream, open the hardware, and program.

```
pyvado_cli --all
```

## TODO
 * Synthesis/implementation/bitstream dependency awareness 
 * Installation via pip
 * More configuration (e.g. number of jobs)
