#!/usr/bin/env python3

# Copyright (c) 2019 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
import sys

import pyvado

parser = argparse.ArgumentParser()
parser.add_argument('--project', type=str, default='.',
                    help='Path to project directory')
parser.add_argument('--xpr', type=str, help='Name of Vivado project file')
parser.add_argument('--synth', action='store_true', help='Run synthesis')
parser.add_argument('--impl', action='store_true', help='Run implementation')
parser.add_argument('--bit', action='store_true', help='Generate bitstream')
parser.add_argument('--device', type=str, help='Device name')
parser.add_argument('--program', action='store_true', help='Program device')
parser.add_argument('--bitfile', type=str, help='Bitstream file location')
parser.add_argument('--all', action='store_true',
                    help='Synthesis, implementation, bitstream, program')
args = parser.parse_args()

if not (args.synth or args.impl or args.bit or args.program or args.all):
    print('Nothing to do!')
    sys.exit(0)

kw = {}
kw['project'] = args.project
kw['project_xpr'] = args.xpr
# If no device specified, let pyvado choose a default
if args.device:
    kw['device'] = args.device
p = pyvado.pyvado(**kw)
p.open_project()

if args.synth or args.all:
    p.synthesis()

if args.impl or args.all:
    p.implementation()

if args.bit or args.all:
    p.bitstream()

if args.program or args.all:
    p.program(args.bitfile)
